# Serverless Shibboleth Starter

### A single page web serverless application reference project with authentication support for UA NetIDs.

This project will help you get started with deploying a serverless application with authentication
support for UA Shibboleth / Webauth. This will let you securely authenticate Lambda functions behind
an AWS API Gateway. 

The AWS Cognito service handles the heavy lifting of integrating with Shibboleth, and acting as a 
SAML service provider. In return your application can authenticate with the Cognito service using 
JWT tokens. Once a JWT token is obtained, it can be used to call APIs behind the API Gateway service,
which also integrates with Cognito to authenticate your JWT token.

This allows for a secure way to authenticate users and be confident that the user information you 
obtain within your Lambda function can be trusted. Once your Lambda function has access to the 
authentication information, you can perform whatever other actions or authorization checks you want.

### Contributors

* A lot of the foundation work for this was done by the [University of Washington][uw]
* Gary Windham, Brett Bendickson, Joe Parsons all helped with brainstorming and implementation details
* [Mark Fischer][] packaged it all up with CloudFormation templates and this deployment guide
* The [UADigital][] working group provided an excellent web template for the design

[uw]: https://github.com/UWFosterIT/aws-cognito/wiki
[UADigital]: http://uadigital.arizona.edu/ua-bootstrap/
[Mark Fischer]: https://github.com/estranged42/

### Assumptions

* You have an AWS account and requisite permissions
* You have basic experience with [AWS S3 and uploading files][s3]
* You have basic understanding of [deploying AWS CloudFormation][cf] templates
* You have experience with Javascript and AJAX/API concepts
* You have experience with HTML/CSS

[s3]: https://docs.aws.amazon.com/AmazonS3/latest/user-guide/upload-objects.html
[cf]: https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/cfn-console-create-stack.html

### Steps

#### Deploy Cognito User Pool
1. Deploy the `serverless-shib-starter.yaml` CloudFormation template. This will create the following
resources:
    * AWS Cognito User Pool
    * Cognito App Client
    * Lambda function for Cognito Pool custom attributes

#### Request Shibboleth Integration
1. Once the Cognito Pool has been created, look at the outputs from the stack. 
    * Make note of the User Pool ID.
    * Make note of the Cognito Domain URL
1. Go to the [UA IAM Application management console][appsiam].
    1. Login and go to the Shibboleth tag
    2. Request new Shibboleth Access
    3. Enter your app name
    4. Copy the Cognito Domain into the application URLs
    5. In the description field, indicate that this is for an AWS Cognito user pool, and provide the User Pool ID.

[appsiam]: https://apps.iam.arizona.edu

#### Deploy App Skeleton
4. Once the UA SIA team has configured Shibboleth, you can deploy the remaining components
5. Deploy the `app.yaml` CloudFormation template. This will create the following resources:
    * S3 bucket to hold your application files
    * Lambda function
    * API Gateway
    * Cloudwatch Log Group
6. Edit the `site/js/app.js` file from this project and update the `config` object at the top with values from your deployment.
7. Copy the contents of the `site` folder from this project into your S3 bucket.



