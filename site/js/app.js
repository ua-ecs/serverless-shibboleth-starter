
var config = {
  userPoolRegion: 'us-west-2',
  userPoolId: 'us-west-2_4UmY6lw0v',
  clientId: '12l70vrtbvekcjd0tq8e8e346n',
  domain: 'ua-serverless-shib.auth.us-west-2.amazoncognito.com',
  signinRedirect: 'https://ua-serverless-shib.uits-nonprod-aws.arizona.edu/app.html',
  apiurl: 'https://knwyx0zzo1.execute-api.us-west-2.amazonaws.com/Prod'
};

function initCognitoSDK() {
  var authData = {
    ClientId: config.clientId,
    AppWebDomain: config.domain,
    TokenScopesArray: ['openid', 'profile', 'email', 'phone'],
    RedirectUriSignIn: config.signinRedirect,
    RedirectUriSignOut: 'https://ua-serverless-shib.uits-nonprod-aws.arizona.edu'
  };
  var auth = new AmazonCognitoIdentity.CognitoAuth(authData);
  auth.userhandler = {
    onSuccess: function(result) {
      console.log('Sign in success', result);
    },
    onFailure: function(error) {
      console.error('Sign in error', error);
    }
  };
  // The default response_type is "token", uncomment the next line to make it be "code" instead.
  // auth.useCodeGrantFlow();
  return auth;
}
// Perform user operations.
function login() {
  var state = $('#loginLink').innerHTML;
  if (state === "Sign Out") {
    $('loginLink').innerHTML = "Sign In";
    auth.signOut();
    clearTokens();
  } else {
    auth.getSession();
  }
}

function currentSession() {
  var session = auth.getSignInUserSession();
  // the actual user
  var idToken = session.getIdToken().getJwtToken();
  if (idToken) {
    var payload = idToken.split('.')[1];
    var formatted = JSON.parse(atob(payload));
    console.log('Id Token Info', formatted);
  }
  var accessToken = session.getAccessToken().getJwtToken();
  if (accessToken) {
    var payload = accessToken.split('.')[1];
    var formatted = JSON.parse(atob(payload));
    console.log('Access Token Info', formatted);
  }
  var refreshToken = session.getRefreshToken().getToken();
  if (refreshToken) {
    var payload = refreshToken.split('.')[1];
    var formatted = JSON.parse(atob(payload));
    console.log('Refresh Token Info', formatted);
  }
}

function apiGateway(apiurl, successFunc) {
  var cognitoUser = auth.getSignInUserSession();
  var token = cognitoUser.idToken.jwtToken;
  $.ajax({
    url: apiurl,
    dataType: "json",
    contentType: 'application/json',
    beforeSend: function(request) {
      request.setRequestHeader("Authorization", token);
    },
    success: successFunc,
    error: function(xhr, textStatus, error) {
      console.log('API ERROR', error);
      console.log(textStatus, xhr);
    }
  });
}

function appStart() {
  apiGateway(config.apiurl, 
    function(data, textStatus, xhr) {
      console.log('API body', data);
      var user = data.user;
      var welcomeMsg = "Welcome " + user.name
      $('#userContainer').text(welcomeMsg)
      var s1 = JSON.stringify( user, null, '\t');
      $('#responseContainer').text( s1 );
      console.log(welcomeMsg);
    }
  );
}



